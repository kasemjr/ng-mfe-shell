import { LoadRemoteModuleOptions } from '@angular-architects/module-federation';

export const environment = {
  production: true,
  mfe: {
    home: {
      remoteName: 'ngMfeAppHome',
      remoteEntry: '/ng-mfe-app-home/remoteEntry.js',
      exposedModule: 'mfe-module'
    } as LoadRemoteModuleOptions
  }
};
